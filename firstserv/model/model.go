package model

type DBConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

type Res struct {
	UserName    string
	ServiceName string
	Count       int
}
