package repository

import (
	"context"
	"github.com/jackc/pgx/v4"
	"strconv"
	"testwork1/firstserv/model"
)

var GetIdUser = `SELECT u.name AS user_name, s.name AS service_name, st.count FROM stats st JOIN users u ON st.user_id = u.id JOIN services s ON st.service_id = s.id WHERE u.id = $1`

var GetIdSevice = `SELECT u.name AS user_name, s.name AS service_name, st.count FROM stats st JOIN users u ON st.user_id = u.id JOIN services s ON st.service_id = s.id WHERE s.id = $1`

const PostCall string = `INSERT INTO stats (user_id, service_id, count) VALUES ($1, $2, 1) ON CONFLICT (user_id, service_id) DO UPDATE SET count = stats.count + EXCLUDED.count`
const PostService string = `INSERT INTO services (name, description) VALUES ($1, $2)`

type Repository struct {
	conn *pgx.Conn
}

func New(conn *pgx.Conn) (*Repository, error) {
	return &Repository{conn: conn}, nil
}

func (r *Repository) GetId(ctx context.Context, id string, offset, limit uint32) (*[]model.Res, error) {
	query := GetIdUser
	args := []interface{}{id}
	if limit > 0 {
		query += " LIMIT $" + strconv.Itoa(len(args)+1)
		args = append(args, limit)
	}

	if offset > 0 {
		query += " OFFSET $" + strconv.Itoa(len(args)+1)
		args = append(args, offset)
	}

	rows, err := r.conn.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var result []model.Res

	for rows.Next() {
		var buf = model.Res{}
		if err = rows.Scan(&buf.UserName, &buf.ServiceName, &buf.Count); err != nil {
			return nil, err
		}
		result = append(result, buf)
	}

	return &result, nil
}

func (r *Repository) GetServiceId(ctx context.Context, id string, limit, offset uint32) (*[]model.Res, error) {
	query := GetIdSevice
	args := []interface{}{id}
	if limit > 0 {
		query += " LIMIT $" + strconv.Itoa(len(args)+1)
		args = append(args, limit)
	}

	if offset > 0 {
		query += " OFFSET $" + strconv.Itoa(len(args)+1)
		args = append(args, offset)
	}

	rows, err := r.conn.Query(ctx, query, args...)
	var result []model.Res

	for rows.Next() {
		var buf = model.Res{}
		if err = rows.Scan(&buf.UserName, &buf.ServiceName, &buf.Count); err != nil {
			return nil, err
		}
		result = append(result, buf)
	}

	return &result, nil
}
func (r *Repository) PostCall(ctx context.Context, userid, serviceid string) error {
	tx, err := r.conn.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, PostCall, userid, serviceid)
	if err != nil {
		return err
	}

	err = tx.Commit(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) PostService(ctx context.Context, name, text string) error {
	tx, err := r.conn.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, PostService, name, text)
	if err != nil {
		return err
	}
	err = tx.Commit(ctx)
	if err != nil {
		return err
	}

	return nil
}
