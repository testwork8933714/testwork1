package config

import (
	"context"
	"github.com/go-ozzo/ozzo-validation/v4"
	"github.com/jackc/pgx/v4"
	"gopkg.in/yaml.v2"
	"os"
	"testwork1/firstserv/model"
)

const cfg = "config/config.yaml"

type Config struct {
	Service    Service    `yaml:"service"`
	PostgreSql PostgreSql `yaml:"postgresql"`
}
type Service struct {
	Address string `yaml:"address"`
	Network string `yaml:"network"`
}

type PostgreSql struct {
	Host      string `yaml:"host"`
	Port      string `yaml:"port"`
	Pass      string `yaml:"pass"`
	User      string `yaml:"user"`
	Databases string `yaml:"databases"`
}

func New() (c *Config, err error) {
	file, err := os.ReadFile(cfg)
	if err != nil {
		return nil, err
	}

	if err = yaml.Unmarshal(file, &c); err != nil {
		return nil, err
	}

	if err = c.Validate(); err != nil {
		return nil, err
	}
	return c, nil
}
func (c *Config) ConnectionDb() (*pgx.Conn, error) {
	connection := model.DBConfig{
		Host:     c.PostgreSql.Host,
		Port:     c.PostgreSql.Port,
		User:     c.PostgreSql.User,
		Password: c.PostgreSql.Pass,
		Database: c.PostgreSql.Databases,
	}

	connStr := "postgresql://" + connection.User + ":" + connection.Password + "@" + connection.Host + ":" + connection.Port + "/" + connection.Database + "?sslmode=disable"

	conn, err := pgx.Connect(context.Background(), connStr)
	if err != nil {
		return nil, err
	}

	err = conn.Ping(context.Background())
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func (c *Config) Validate() error {
	return validation.Errors{
		"service":    validation.Validate(c.Service, validation.Required),
		"postgresql": validation.Validate(c.PostgreSql, validation.Required),
	}.Filter()
}

func (s Service) Validate() error {
	return validation.Errors{
		"address": validation.Validate(s.Address, validation.Required),
		"network": validation.Validate(s.Network, validation.Required),
	}.Filter()
}

func (p PostgreSql) Validate() error {
	return validation.Errors{
		"host":      validation.Validate(p.Host, validation.Required),
		"port":      validation.Validate(p.Port, validation.Required),
		"pass":      validation.Validate(p.Pass, validation.Required),
		"user":      validation.Validate(p.User, validation.Required),
		"databases": validation.Validate(p.Databases, validation.Required),
	}.Filter()
}
