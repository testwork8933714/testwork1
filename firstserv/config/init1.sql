CREATE TABLE IF NOT EXISTS services (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description TEXT
    );

CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
    );

CREATE TABLE IF NOT EXISTS stats (
    user_id INTEGER REFERENCES users(id),
    service_id INTEGER REFERENCES services(id),
    count INTEGER DEFAULT 0,
    PRIMARY KEY (user_id, service_id)
    );
