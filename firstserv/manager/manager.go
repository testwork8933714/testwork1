package manager

import (
	"context"
	"testwork1/firstserv/model"
	"testwork1/firstserv/repository"
)

type Manager struct {
	repository *repository.Repository
}

func New(r *repository.Repository) (*Manager, error) {
	return &Manager{repository: r}, nil
}

func (m *Manager) GetID(ctx context.Context, id string, offset, limit uint32) (*[]model.Res, error) {
	return m.repository.GetId(ctx, id, offset, limit)
}

func (m *Manager) GetServiceID(ctx context.Context, id string, limit, offset uint32) (*[]model.Res, error) {
	return m.repository.GetServiceId(ctx, id, limit, offset)
}

func (m *Manager) PostCall(ctx context.Context, userid, servicid string) error {
	return m.repository.PostCall(ctx, userid, servicid)
}

func (m *Manager) PostService(ctx context.Context, name, text string) error {
	return m.repository.PostService(ctx, name, text)
}
