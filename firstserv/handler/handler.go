package handler

import (
	"context"
	"google.golang.org/grpc"
	"net"
	"strconv"
	"testwork1/firstserv/config"
	"testwork1/firstserv/manager"
	pb "testwork1/firstserv/protoconfig"
)

type Handler struct {
	manager *manager.Manager
}

func New(m *manager.Manager) (*Handler, error) {
	return &Handler{manager: m}, nil
}

func (h *Handler) GetId(ctx context.Context, id *pb.GetID) (*pb.ResAllList, error) {

	result, err := h.manager.GetID(ctx, id.UserId, id.Offset, id.Limit)
	if err != nil {
		return nil, err
	}
	var resList pb.ResAllList
	for _, r := range *result {
		resList.ResAll = append(resList.ResAll, &pb.ResAll{
			UserName:    r.UserName,
			ServiceName: r.ServiceName,
			Count:       strconv.Itoa(r.Count),
		})
	}
	return &resList, nil
}

func (h *Handler) GetServiceID(ctx context.Context, id *pb.GetServicesID) (*pb.ResAllList, error) {
	result, err := h.manager.GetServiceID(ctx, id.SId, id.Limit, id.Offset)
	if err != nil {
		return nil, err
	}
	var resList pb.ResAllList
	for _, r := range *result {
		resList.ResAll = append(resList.ResAll, &pb.ResAll{
			UserName:    r.UserName,
			ServiceName: r.ServiceName,
			Count:       strconv.Itoa(r.Count),
		})
	}
	return &resList, nil
}

func (h *Handler) PostC(ctx context.Context, call *pb.PostCall) (*pb.Null, error) {
	return nil, h.manager.PostCall(ctx, call.UserId, call.ServiceId)
}

func (h *Handler) PostS(ctx context.Context, services *pb.PostServices) (*pb.Null, error) {
	return nil, h.manager.PostService(ctx, services.Name, services.Text)
}

func (h *Handler) StartApi(c config.Service) error {
	lis, err := net.Listen(c.Network, c.Address)
	if err != nil {
		return err
	}

	s := grpc.NewServer()
	pb.RegisterAuthServiceServer(s, h)
	if err := s.Serve(lis); err != nil {
		return err
	}
	return nil
}
