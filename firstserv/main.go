package main

import (
	"github.com/rs/zerolog/log"
	"testwork1/firstserv/config"
	"testwork1/firstserv/handler"
	"testwork1/firstserv/manager"
	"testwork1/firstserv/repository"
)

func main() {
	err := app()
	if err != nil {
		log.Fatal().Err(err).Send()
	}
}

func app() error {
	c, err := config.New()
	if err != nil {
		return err
	}
	conn, err := c.ConnectionDb()
	if err != nil {
		return err
	}

	r, err := repository.New(conn)
	if err != nil {
		return err
	}

	m, err := manager.New(r)
	if err != nil {
		return err
	}
	h, err := handler.New(m)
	if err != nil {
		return err
	}

	return h.StartApi(c.Service)
}
