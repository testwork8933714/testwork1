package repository

import (
	"context"
	"google.golang.org/grpc"
	pb "testwork1/protoconfig"
)

type Repository struct {
	conn   *grpc.ClientConn
	client pb.AuthServiceClient
}

func New(conn *grpc.ClientConn) (*Repository, error) {
	client := pb.NewAuthServiceClient(conn)
	return &Repository{conn: conn, client: client}, nil
}

func (r *Repository) GetService(ctx context.Context, serviceId *string, offset, limit uint32) (*pb.ResAllList, error) {
	result, err := r.client.GetServiceID(ctx, &pb.GetServicesID{SId: *serviceId, Limit: limit, Offset: offset})
	if err != nil {
		return nil, err
	}
	return result, err
}

func (r *Repository) GetUser(ctx context.Context, userID *string, offset, limit uint32) (*pb.ResAllList, error) {
	result, err := r.client.GetId(ctx, &pb.GetID{UserId: *userID, Limit: limit, Offset: offset})
	if err != nil {
		return nil, err
	}
	return result, err
}

func (r *Repository) Cals(ctx context.Context, userID, serviceID string) error {
	_, err := r.client.PostC(ctx, &pb.PostCall{UserId: userID, ServiceId: serviceID})
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) Service(ctx context.Context, name, text string) error {
	_, err := r.client.PostS(ctx, &pb.PostServices{Name: name, Text: text})
	if err != nil {
		return err
	}
	return nil
}
