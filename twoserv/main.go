package main

import (
	"context"
	"github.com/rs/zerolog/log"
	"os"
	"os/signal"
	"testwork1/config"
	"testwork1/handler"
	"testwork1/manager"
	"testwork1/repository"
)

func main() {
	err := app()
	if err != nil {
		log.Fatal().Err(err).Send()
	}
}

func app() error {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	c, err := config.New()
	if err != nil {
		return err
	}

	connGrpc, err := c.ConnectGrpc(c.GrpcService.Address)
	if err != nil {
		return err
	}

	r, err := repository.New(connGrpc)
	if err != nil {
		return err
	}

	m, err := manager.New(r)
	if err != nil {
		return err
	}

	h, err := handler.New(m)
	if err != nil {
		return err
	}
	return h.StartApi(ctx, c.EchoService.Address, c.EchoService.Port)

}
