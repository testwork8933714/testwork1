package model

type Get struct {
	UserID    *string `query:"user_id"`
	ServiceID *string `query:"service_id"`
	Offset    uint32  `query:"offset"`
	Limit     uint32  `query:"limit"`
}
type Result struct {
	UserName    string
	ServiceName string
	Count       int
}

type Cals struct {
	UserId    string `query:"user_id"`
	ServiceId string `query:"service_id"`
}

type Service struct {
	Name string `param:"name" query:"name" form:"name" json:"name" xml:"name"`
	Text string `param:"text" query:"text" form:"text" json:"text" xml:"text"`
}
