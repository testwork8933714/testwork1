package config

import (
	"github.com/go-ozzo/ozzo-validation/v4"
	"google.golang.org/grpc"
	"gopkg.in/yaml.v3"
	"os"
)

const cfg = "config/config.yaml"

type Config struct {
	EchoService EchoService `yaml:"echo-service"`
	GrpcService GrpcService `yaml:"grpc"`
}

type EchoService struct {
	Address string `yaml:"address"`
	Port    string `yaml:"port"`
}

type GrpcService struct {
	Address string `yaml:"address"`
}

func New() (c *Config, err error) {
	file, err := os.ReadFile(cfg)
	if err != nil {
		return nil, err
	}
	if err = yaml.Unmarshal(file, &c); err != nil {
		return nil, err
	}
	if err = c.Validate(); err != nil {
		return nil, err
	}
	return c, nil
}

func (c *Config) ConnectGrpc(address string) (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	return conn, nil
}

func (c *Config) Validate() error {
	return validation.Errors{
		"echo-service": validation.Validate(c.EchoService, validation.Required),
		"grpc":         validation.Validate(c.GrpcService, validation.Required),
	}.Filter()
}

func (e EchoService) Validate() error {
	return validation.Errors{
		"addres": validation.Validate(e.Address, validation.Required),
		"port":   validation.Validate(e.Port, validation.Required),
	}.Filter()
}

func (g GrpcService) Validate() error {
	return validation.Errors{
		"address": validation.Validate(g.Address, validation.Required),
	}.Filter()
}
