package handler

import (
	"context"
	"github.com/labstack/echo/v4"
	"net/http"
	"testwork1/manager"
	"testwork1/model"
	pb "testwork1/protoconfig"
)

type Handler struct {
	manager *manager.Manager
}

func New(m *manager.Manager) (*Handler, error) {
	return &Handler{manager: m}, nil
}

func (h *Handler) StartApi(ctx context.Context, address, port string) error {
	e := echo.New()
	e.HideBanner = true
	e.HidePort = true

	e.GET("/calls", func(e echo.Context) error {
		var (
			result = &pb.ResAllList{}
			u      = new(model.Get)
		)
		err := e.Bind(u)
		if err != nil {
			return e.JSON(http.StatusBadRequest, err)
		}

		if u.ServiceID != nil {
			result, err = h.manager.GetService(ctx, u.ServiceID, u.Offset, u.Limit)
			if err != nil {
				return e.JSON(http.StatusInternalServerError, err)
			}
		} else {
			result, err = h.manager.GetUser(ctx, u.UserID, u.Offset, u.Limit)
			if err != nil {
				return e.JSON(http.StatusInternalServerError, err)
			}
		}

		return e.JSON(http.StatusOK, result)
	})

	e.POST("/call", func(e echo.Context) error {
		var u = new(model.Cals)
		if err := e.Bind(u); err != nil {
			return e.JSON(http.StatusBadRequest, err)
		}
		if err := h.manager.Cals(ctx, u.UserId, u.ServiceId); err != nil {
			return e.JSON(http.StatusInternalServerError, err)
		}
		return e.JSON(http.StatusOK, "ok")
	})

	e.POST("/service", func(e echo.Context) error {
		var u = new(model.Service)
		if err := e.Bind(u); err != nil {
			return e.JSON(http.StatusBadRequest, err)
		}
		if err := h.manager.Service(ctx, u.Name, u.Text); err != nil {
			return e.JSON(http.StatusInternalServerError, err)
		}
		return e.JSON(http.StatusOK, "ok")
	})

	go func() {
		e.Logger.Fatal(e.Start(address + ":" + port))
	}()
	<-ctx.Done()

	return nil
}
