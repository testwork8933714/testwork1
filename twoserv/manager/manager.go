package manager

import (
	"context"
	pb "testwork1/protoconfig"
	"testwork1/repository"
)

type Manager struct {
	repository *repository.Repository
}

func New(r *repository.Repository) (*Manager, error) {
	return &Manager{repository: r}, nil
}

func (m *Manager) GetService(ctx context.Context, serviceId *string, offset, limit uint32) (*pb.ResAllList, error) {
	return m.repository.GetService(ctx, serviceId, offset, limit)
}

func (m *Manager) GetUser(ctx context.Context, userId *string, offset, limit uint32) (*pb.ResAllList, error) {
	return m.repository.GetUser(ctx, userId, offset, limit)
}

func (m *Manager) Cals(ctx context.Context, userId, serviceID string) error {
	return m.repository.Cals(ctx, userId, serviceID)
}

func (m *Manager) Service(ctx context.Context, name, text string) error {
	return m.repository.Service(ctx, name, text)
}
